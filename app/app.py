from flask import Flask, jsonify, request, abort
import json

# The main Flask app
app = Flask(__name__)

# Data from a json file
data = json.load(open('coe332.json', 'r'))

@app.route('/')
def coe332():
    return jsonify(data)

@app.route('/meeting')
def get_meeting():
    return jsonify(data['meeting'])

@app.route('/meeting/<info>')
def get_info(info):
    return jsonify(data['meeting'][info])

@app.route('/instructors')
def get_instructors():
    return jsonify(data['instructors'])

@app.route('/instructors/<int:number>')
def get_instructor_by_id(number):
    if number>=3:
        abort(404)
    else:
        return jsonify(data['instructors'][number])

@app.route('/assignments', methods=['GET',])
def assignments_get():
    return jsonify(data['assignments'])
    
@app.route('/assignments', methods=['POST'])    
def assignments_post():
    request_data = request.data.decode('utf-8')
    request_dictionary = json.loads(request_data)
    data['assignments'].append(request_dictionary)
    return jsonify(request_dictionary)

@app.route('/assignments/<int:number>/<info>')
def get_assignment_info(number, info):
    return jsonify(data['assignments'][number][info])
